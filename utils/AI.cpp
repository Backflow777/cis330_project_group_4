#include "AI.hpp"

// Function used to set up the pieces
int AI::placement(ElementType e, int x, int ry, int by, int i)
{   
    
    if (board->get_team() == Team::red)
    {
        board->place(e, x, ry);
        return ++i;
    }
    if (board->get_team() == Team::blue)
    {
        board->place(e, x, by);
        return ++i;
    }
    return i;
}

void AI::setup_pieces(bool start)
{   
    if (!start)
    {
        std::cout << "Require a starting signal!" << std::endl;
        exit(EXIT_FAILURE);
    } 
    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::uniform_int_distribution<> red_blue_x(0, 9);
    std::uniform_int_distribution<> red_y(0, 2);
    std::uniform_int_distribution<> blue_y(7, 9);
    
    for (const ElementType e : {ElementType::spy,ElementType::scout,ElementType::miner,ElementType::nine,ElementType::ten,ElementType::bomb,ElementType::flag})
    {
        int i = 0, rb_x, ry, by;
        if ((e == ElementType::bomb || e == ElementType::scout || e == ElementType::miner))
        {
            while (i < 2)
            { 
                rb_x = red_blue_x(gen);
                ry = red_y(gen);
                by = blue_y(gen);              
                if ((e == ElementType::scout) && (ry >= 1 || by <= 8))
                {
                    i = placement(e, rb_x, ry, by, i);
                }
                else if ((e == ElementType::bomb || e == ElementType::miner) && (ry <= 1 || by >= 8))
                {
                    i = placement(e, rb_x, ry, by, i);
                }
            }
            i = 0;
        }
        else 
        {
            while (i < 1)
            {
                rb_x = red_blue_x(gen);
                ry = red_y(gen);
                by = blue_y(gen);              
                if (e == ElementType::spy)
                {
                    i = placement(e, rb_x, ry, by, i);
                }
                else if ((e == ElementType::nine || e == ElementType::ten) && (ry >= 1 || by <= 8))
                {
                    i = placement(e, rb_x, ry, by, i);
                }
                else if ((e == ElementType::flag) && (ry <= 1 || by >= 8))
                {
                    i = placement(e, rb_x, ry, by, i);
                }
            }
            i = 0;
        }
    }
    mmt = new minimaxTree();
    std::cout << "Set!" << std::endl;
}

    // Function used to start a Player's turn.
    // The turn ends when the function resolves.
void AI::start_turn()
{
    if (board->get_team() == Team::red)
    {
        enemy_spmr = board->return_spmr(Team::blue,true);
        allies_spmr = board->return_spmr(Team::red,false);
    }
    else if (board->get_team() == Team::blue)
    {
        enemy_spmr = board->return_spmr(Team::red,true);
        allies_spmr = board->return_spmr(Team::blue,false);
    }
    
    //move final_move = mmt->final_move();
    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::uniform_int_distribution<> move(0, 9);
    std::uniform_int_distribution<> dir(0, 3);
    int z = move(gen);
    int k = dir(gen);
    for (int i = z; i == z; ++i)
    {
        int y = allies_spmr.spmr[0][z];
        int x = allies_spmr.spmr[1][z];
        
        switch (k)
        {
        case static_cast<int>(Direction::down):
            board->move(x,y,Direction::down,1);
            break;
        case static_cast<int>(Direction::up):
            board->move(x,y,Direction::up,1);
            break;
        case static_cast<int>(Direction::left):
            board->move(x,y,Direction::left,1);
            break;
        case static_cast<int>(Direction::right):
            board->move(x,y,Direction::right,1);
            break;
        default:
            break;
        }      
    }
    
    /*
    bool success = mmt->compare(enemy_spmr,allies_spmr);
    mmt->build_tree(success);
    //mmt->build_tree(true);

    move final_move = mmt->move_eval();
    if (final_move.move_data != Direction::unknown)
    {
        std::cout << final_move.x << " " << final_move.y << " " << static_cast<int>(final_move.move_data) << std::endl;
        board->move(final_move.x,final_move.y,final_move.move_data,final_move.space);
    }
     */
    //Node *temp = new Node();
    //mmt->build_from(temp);

    std::cout << "Turn!" << std::endl;
}

    // Virtual destructor
AI::~AI() 
{
    delete mmt;
    std::cout << "End!" << std::endl;
}
