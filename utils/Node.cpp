#include "Node.hpp"
#include <stdlib.h>
#include <iostream>
#include <cstdlib>
#include <climits>
#include <iostream>
Node::Node()
{
    child_array = new Node*[400];
    for (int i = 0; i < 400; ++i) child_array[i] = nullptr;
    NumOfChildren = 0;
    parent = nullptr;
    max_child = 400;
}
Node::Node(Node *parent_node, COO current_turn_us, COO current_turn_foe, int num_child, move &movement, int total_score, bool signal)
{
    // check setup error
    if (!(node_setup(parent_node, current_turn_us, current_turn_foe, movement, total_score, signal)))
    {
        std::cout << "Node Setup Failure" << std::endl;
        exit(EXIT_FAILURE);
    };

    // generate child nodes
    if (num_child > 0) 
    {
        if (num_child > 400)
        {
            std::cout << "Number of Child Exceed 40" << std::endl;
            exit(EXIT_FAILURE);
        }
        child_array = new Node*[num_child];
        for (int i = 0; i < num_child; ++i) child_array[i] = nullptr;
        this->NumOfChildren = 0;
        max_child = num_child;
    }
    else
    {
        std::cout << "No Child" << std::endl;
        child_array = nullptr;
    }
}
bool Node::node_setup(COO current_turn, move &movement, int total_score, bool signal)
{
    if (signal)
    {
        turn_us = current_turn;
    }
    else
    {
        turn_foe = current_turn;
    }
    piece_movement = movement;
    piece_score = total_score;
    node_eval();
}
bool Node::node_setup(Node *parent_node, COO current_turn_us, COO current_turn_foe, move &movement, int total_score, bool signal)
{   
    try
    {
        parent = parent_node;   // default as null pointer to its parent
        turn_us = current_turn_us;    // receive current turn board information in COO
        turn_foe = current_turn_foe; // receive current foe`s turn board information in COO
        if (signal) 
        {
            turn = turn_us;
        }
        else
        {
            turn =turn_foe;
        }
        
        piece_movement = movement;  // what piece the tree decide to move

        piece_score = total_score;  // what the total tree score is

        node_eval();
    }
    catch(...)
    {
        std::cout << "Node Setup Failure" << std::endl;
        return false;
    }
    return true;
}

void Node::node_eval()
{
    // set calculation coordinates for the planned move
    int x,y;
    switch(piece_movement.move_data)
    {
        case (Direction::down):
            y = piece_movement.y - 1; 
            x = piece_movement.x;
            break;
        case (Direction::up):
            y = piece_movement.y + 1; 
            x = piece_movement.x;
            break;
        case (Direction::left):
            y = piece_movement.y;
            x = piece_movement.x - 1;
            break;
        case (Direction::right):
            y = piece_movement.y;
            x = piece_movement.x + 1;
            break;
        default:
            return;
    }
    
    int surr_piece[SURR_ITEMS][LOCAL_ITEMS];
    int index = 0;
    for (int i = 0; i < 10; ++i)
    {
        // iterate through current turn of the enemy spmr to see whether it's around allie piece or not after the move
        if ((turn.spmr[0][i] <= (y + 1) && turn.spmr[0][i] >= (y - 1)) && (turn.spmr[1][i] <= (x + 1) && turn.spmr[1][i] >= (x - 1)))
        {   
            // if it is around the piece, store its y and x coordinate vaoue
            surr_piece[index][0] = turn.spmr[0][i]; // y coordinate value
            surr_piece[index][1] = turn.spmr[1][i]; // x coordinate value

            // iterate through known-enemy-piece spmr to see whether it's around allie piece or not after the move
            for (int j = 0; j < 10; ++j)
            {
                if (turn.spmr[0][i] == known_piece.spmr[0][j] && turn.spmr[1][i] == known_piece.spmr[1][j] && known_piece.spmr[2][j] != 9)
                {
                    surr_piece[index][2] = known_piece.spmr[2][j]; // if it is known, use the relative value of the enum piece
                }
                else
                {
                    surr_piece[index][2] = 9; // if it is not known, assume it as unknown, enum number nine
                }
            }            
            ++index;
        }
    }
    for (; index < SURR_ITEMS; ++index) surr_piece[index][2] = (-1); // if there are space left in the surround piece array, set it to negative 1

    piece_score += node_comp(x, y, surr_piece);
}

int Node::node_comp(int x, int y, int arr[SURR_ITEMS][LOCAL_ITEMS])
{
    /*
    1. A move that results in a direct win, capturing the opponent’s Flag.
    2. An attacking move with a sure win.
    3. A move attacking an unknown piece.
    4. A move towards the opponent
    5. A move sideways
    6. A move backwards
    7. An attacking move with a sure loss
    */
    int x1, y1;
    double score = 0;

    for (int i = 0; i < SURR_ITEMS; ++i)
    {
        if (arr[i][2] == (-1)) break;
        y1 = arr[i][0] - y;
        x1 = arr[i][1] - x;
        int distance = std::abs(y1) + std::abs(x1);

        if (distance == 2) score += 0.5;
        if (distance == 1) score += 1;
        if (distance == 0)
        {   
            if (arr[i][2] == 9)
            {
                score += 2;
                continue;
            }
            for (int j = 0; j < 10; ++j)
            {
                if (static_cast<int>(piece_movement.piece) == known_piece.spmr[2][j])
                {
                    --score;
                }
                else if (static_cast<int>(piece_movement.piece) <= known_piece.spmr[2][j])
                {
                    score -= 2;
                }
                else if (static_cast<int>(piece_movement.piece) >= known_piece.spmr[2][j])
                {
                    score += 2;
                }
     
            }
        }
    }
    return score;
}

Node ** Node::getChildren()
{
    return this->child_array;
}
void Node::swap(Node **a, Node **b)
{
    Node *temp1 = *a;
    Node *temp2 = *b;
    *a = temp2;
    *b = temp1;
}

int Node::partition(int low, int high)
{
    Node *pivot = this->child_array[high];
    int i = low - 1;
    for (int j = low; j < high - 1; j++)
    {
        if (this->child_array[j]->piece_score <= pivot->piece_score)
        {
            i++;
            swap(&(this->child_array[i]), &(this->child_array[j]));

        }
    }
    swap(&(this->child_array[i + 1]), &(this->child_array[high]));
    return (i + 1);
}

void Node::QuickSort(int low, int high)
{
    if (low < high)
    {
        int pi = this->partition(low, high);
        this->QuickSort(low, pi - 1);
        this->QuickSort(pi + 1, high);
    }
    
}

void Node::addChild(Node *newChild)
{
    if (NumOfChildren >= max_child)
    {
        std::cout << "Number of Child Exceed: "<< max_child << std::endl;
        exit(EXIT_FAILURE);
    }
    this->child_array[this->NumOfChildren] = newChild;
    this->NumOfChildren++;
    newChild->parent = this;
    this->QuickSort(0, this->NumOfChildren - 1);
}
int Node::ShowChildrenNum()
{
    return this->NumOfChildren;
}

bool Node::compare_by_spmr(COO spmr)
{
    bool result = true;
    for (int i = 0; i < 10; i++)
    {
        if (spmr.spmr[0][i] != this->turn.spmr[0][i])
        {
            result = false;
        }
        if (spmr.spmr[1][i] != this->turn.spmr[1][i])
        {
            result = false;
        }
        if (spmr.spmr[2][i] != this->turn.spmr[2][i])
        {
            result = false;
        }
    }
    return result;
    
}
COO Node::show_turn_us()
{
    return this->turn_us;
}
COO Node::show_turn_foe()
{
    return this->turn_foe;
}
int Node::show_score()
{
    return this->piece_score;
}

Node::~Node()
{
    if (child_array != nullptr)
    {
        for (int i = 0; i < max_child; ++i) 
        {
            if (child_array[i] != nullptr)
            {
                delete child_array[i];
            }
        }
        delete[] child_array;
    }
}
