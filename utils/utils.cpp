#include "utils.hpp"

// Print element type as char
std::ostream & operator<<(std::ostream &s, ElementType piece) {
    char p;
    switch (piece) {
    case ElementType::spy:
        p = '1';
        break;
    case ElementType::scout:
        p = '2';
        break;
    case ElementType::miner:
        p = '3';
        break;
    case ElementType::nine:
        p = '9';
        break;
    case ElementType::ten:
        p = 'A';
        break;
    case ElementType::bomb:
        p = 'B';
        break;
    case ElementType::flag:
        p = 'F';
        break;
    default:
        p = '?';
        break;
    }
    s << p;
    return s;
}