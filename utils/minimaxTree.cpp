#include <stdlib.h>
#include <iostream>

#include "minimaxTree.hpp"

const int MAX = 1000;
const int MIN = -1000;

minimaxTree::~minimaxTree()
{
	if (root) delete root;
}

minimaxTree::minimaxTree()
{
	temp = new Node();
	this->root = temp;
}
minimaxTree::minimaxTree(Node *NewMove)
{
	this->root = NewMove;
}

bool minimaxTree::IsLeaf(Node *the_node)
{
	if (the_node->ShowChildrenNum() == 0)
	{
		return true;
	}
	return false;
}

void minimaxTree::build_base(Node *start_point, Node *NewBase)
{

	for (int i = 0; i < start_point->ShowChildrenNum(); i++)
	{
		if (IsLeaf(start_point->child_array[i]) == true)
		{
			if (start_point->child_array[i]->compare_by_spmr(NewBase->show_turn_us()) != true)
			{
				start_point->child_array[i]->~Node();
			}
		}
		else
		{
			this->build_base(start_point->child_array[i], NewBase);
		}
	}
	Node *temp = this->root;
	this->root = NewBase;
	delete temp;
}

void minimaxTree::delete_node(Node *selected)
{
	for (int i = 0; i < selected->ShowChildrenNum(); i++)
	{
		for (int j = 0; j < selected->child_array[i]->ShowChildrenNum(); j++)
		{
			for (int n = 0; n = selected->child_array[i]->child_array[j]->ShowChildrenNum(); n++)
			{
				delete selected->child_array[i]->child_array[j]->child_array[n];
				delete selected->child_array[i]->child_array[j]->child_array;
			}
			delete selected->child_array[i]->child_array[j];
			delete selected->child_array[i]->child_array;
		}
		delete selected->child_array[i];
		delete selected->child_array;
	}
	delete selected;
}
// search the node to see if the circumstance is our expectation
bool minimaxTree::search_node(COO spmr)
{
	bool result = false;
	this->current = this->root;
	this->second_floor = this->current;
	int iter_num = this->current->ShowChildrenNum();
	Node *NewBase;
	for (int i = 0; i < iter_num; i++)
	{
		this->current = this->second_floor->child_array[i];
		for (int j = 0; j < current->ShowChildrenNum(); j++)
		{
			this->third_floor = current;
			current = this->third_floor->child_array[j];
			if (current->compare_by_spmr(spmr) == true)
			{
				result = true;
				temp = current;
			}
			if (this->third_floor->compare_by_spmr(spmr) == true)
			{
				result = true;
				temp = third_floor;
			}
		}
		if (this->second_floor->child_array[i]->compare_by_spmr(spmr) == true)
		{
			result = true;
			temp = third_floor;
		}
	}
	return result;
}
/*
move minimaxTree::final_move()
{
	if (depth == 0)
}
 */
void minimaxTree::build_from(Node *start_node)
{
	/*static bool turn;
	if (root == nullptr)
	{
		root == new Node();
		if (turn)
		{
			root->node_setup(start_node.show_turn_us());
			turn = false;
		}
		else
		{
			root->node_setup(start_node.show_turn_foe());
			turn = true;
		}
	}*/
	this->root = start_node;
	this->generate_move(start_node, true);
	for (int i = 0; i < start_node->ShowChildrenNum(); i++)
	{
		/* if (start_node->child_array[i]->NumOfChildren > 0) 
		{
			std::cout << start_node->child_array[i]->NumOfChildren << std::endl;
		} */
		this->generate_move(start_node->child_array[i], false);
		for (int j = 0; j < start_node->child_array[i]->ShowChildrenNum(); j++)
		{
			this->generate_move(start_node->child_array[i]->child_array[j], true);
		}
	}

	/*if (root->getChildren() == nullptr)
	{
		COO k = start_node.show_turn_us();
		Node *moves;
		int index = sizeof(moves);
		for (int i = 0; i < index; ++i)
		{
			turn = false;
			start_node.addChild(&moves[i]);
			build_from(moves[i]);
		}
		turn = true;
		return;
	}*/
}
bool minimaxTree::compare(COO enemy, COO allies)
{
	enemy_pieces = enemy;
	allies_pieces = allies;
	if (root == nullptr) // check whether there is a tree at all or not
	{
		enemy_pieces = enemy;
		allies_pieces = allies;
		return false;
	}
	else if (search_node(enemy)) // check whether there is a node with same enemy coo, ths earch should set a temp node for tree building
	{
		return true;
	}
	else // nothing matched data in the tree
	{
		return false;
	}
}
void minimaxTree::build_tree(bool success)
{
	if (success)
	{
		delete_node(temp); // delete everything up to that node
		build_from(temp);   // set the temp node a a root, build the tree
	}
	else
	{
		if (root == nullptr)
		{
			move temp_mov;
			temp->node_setup(allies_pieces, temp_mov, 0, true); // initiate the root node
			build_from(temp);									// build the tree from that node
		}
		else
		{
			move temp_mov;
			delete_node(root);									// delete the entire tree
			temp->node_setup(allies_pieces, temp_mov, 0, true); // initiate the root node
			build_from(temp);									// build the tree from that node
		}
	}
}

move minimaxTree::move_eval()
{
	max_score_node = nullptr;
	max_score = -1000;
	search_move(root, 0);
	if (max_score_node != root)
	{
		while (max_score_node->getParent() != root) max_score_node = max_score_node->getParent();
		return max_score_node->getMove();
	}
	move fail;
	return fail;
};

void minimaxTree::search_move(Node *parent, int index)
{
	if (parent->NumOfChildren > 0)
	{
		for (int i = 0; i < parent->NumOfChildren; ++i)
		{
			search_move(parent->child_array[i], i);
		}
	}
	else
	{
		if (parent->show_score() > max_score)
		{
			max_score = parent->show_score();
			max_score_node = parent;
		}
	}
	
}

// A function just to fill the parent node `s childArray with node.
// I know our way to generate move is kind dumb, but since some pieces in stratego have their own way to optimize the move. Writing
// all of them out there is kind easy to add further changes. At least, I think so. 
void minimaxTree::generate_move(Node *parent, bool IsOurTurn)
{
	COO current_us = parent->show_turn_us();
	COO current_foe = parent->show_turn_foe();

	if (IsOurTurn == true)
	{
		for (int i = 0; i < 10; i++)
		{
			if (current_us.spmr[2][i] != 6 && current_us.spmr[2][i] != 7 && current_us.spmr[2][i] != 8 && current_us.spmr[2][i] != 9 && current_us.spmr[2][i] != 10)
			{
				// coordination of our current piece x and y
				int x = current_us.spmr[0][i];
				int y = current_us.spmr[1][i];
				int l = 4;
				int PieceValue = current_us.spmr[2][i];
				while (l > 0)
				{
					COO A_Move = current_us;
					move NewMove;

					if (l == 4)
					{
						switch (PieceValue)
						{
						case (1):
						{
							NewMove.piece = ElementType::spy;
							NewMove.move_data = Direction::down;
							NewMove.x = x;
							NewMove.y = y - 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y - 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						case (2):
						{
							NewMove.piece = ElementType::scout;
							NewMove.move_data = Direction::down;
							NewMove.x = x;
							NewMove.y = y - 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y - 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (3):
						{
							NewMove.piece = ElementType::miner;
							NewMove.move_data = Direction::down;
							NewMove.x = x;
							NewMove.y = y - 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y - 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (4):
						{
							NewMove.piece = ElementType::nine;
							NewMove.move_data = Direction::down;
							NewMove.x = x;
							NewMove.y = y - 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y - 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (5):
						{
							NewMove.piece = ElementType::ten;
							NewMove.move_data = Direction::down;
							NewMove.x = x;
							NewMove.y = y - 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y - 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						default:
							break;
						}
					}
					if (l == 3)
					{
						switch (PieceValue)
						{
						case (1):
						{
							NewMove.piece = ElementType::spy;
							NewMove.move_data = Direction::up;
							NewMove.x = x;
							NewMove.y = y + 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y + 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						case (2):
						{
							NewMove.piece = ElementType::scout;
							NewMove.move_data = Direction::up;
							NewMove.x = x;
							NewMove.y = y + 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y + 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (3):
						{
							NewMove.piece = ElementType::miner;
							NewMove.move_data = Direction::up;
							NewMove.x = x;
							NewMove.y = y + 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y + 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (4):
						{
							NewMove.piece = ElementType::nine;
							NewMove.move_data = Direction::up;
							NewMove.x = x;
							NewMove.y = y + 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y + 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (5):
						{
							NewMove.piece = ElementType::ten;
							NewMove.move_data = Direction::up;
							NewMove.x = x;
							NewMove.y = y + 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y + 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						default:
							break;
						}
					}
					if (l == 2)
					{
						switch (PieceValue)
						{
						case (1):
						{
							NewMove.piece = ElementType::spy;
							NewMove.move_data = Direction::left;
							NewMove.x = x - 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x - 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						case (2):
						{
							NewMove.piece = ElementType::scout;
							NewMove.move_data = Direction::left;
							NewMove.x = x - 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x - 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (3):
						{
							NewMove.piece = ElementType::miner;
							NewMove.move_data = Direction::left;
							NewMove.x = x - 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x - 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (4):
						{
							NewMove.piece = ElementType::nine;
							NewMove.move_data = Direction::left;
							NewMove.x = x - 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x - 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (5):
						{
							NewMove.piece = ElementType::ten;
							NewMove.move_data = Direction::left;
							NewMove.x = x - 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x - 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						default:
							break;
						}
					}

					if (l == 1)
					{
						switch (PieceValue)
						{
						case (1):
						{
							NewMove.piece = ElementType::spy; 
							NewMove.move_data = Direction::right;
							NewMove.x = x + 1;
							NewMove.y = y;
							Node * NewNode = new Node();
							A_Move.spmr[0][i] = x + 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
							
							
						case (2):
						{
							NewMove.piece = ElementType::scout; 
							NewMove.move_data = Direction::right;
							NewMove.x = x + 1;
							NewMove.y = y;
							Node * NewNode = new Node();
							A_Move.spmr[0][i] = x + 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);	
							break;
						}
						case (3):
						{
							NewMove.piece = ElementType::miner; 
							NewMove.move_data = Direction::right;
							NewMove.x = x + 1;
							NewMove.y = y;
							Node * NewNode = new Node();
							A_Move.spmr[0][i] = x + 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (4): 
						{
							NewMove.piece = ElementType::nine; 
							NewMove.move_data = Direction::right;
							NewMove.x = x + 1;
							NewMove.y = y;
							Node * NewNode = new Node();
							A_Move.spmr[0][i] = x + 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (5):
						{
							NewMove.piece = ElementType::ten; 
							NewMove.move_data = Direction::right;
							NewMove.x = x + 1;
							NewMove.y = y;
							Node * NewNode = new Node();
							A_Move.spmr[0][i] = x + 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
								
						default:
							break;
						}
					}

					l -= 1;
				}
			}
		}
	}
	else
	{ // This move generator still need to improve. Since we cannot know what exactly their piece is , the whole matrix
		// that we are going to have is full of unknown pieces.
		for (int i = 0; i < 10; i++)
		{
			if (current_foe.spmr[2][i] != 6 && current_foe.spmr[2][i] != 7 && current_foe.spmr[2][i] != 8 && current_foe.spmr[2][i] != 10)
			{
				// coordination of opponent`s current piece x and y
				int x = current_foe.spmr[0][i];
				int y = current_foe.spmr[1][i];
				int l = 4;
				int PieceValue = current_foe.spmr[2][i];
				while (l > 0)
				{
					COO A_Move = current_foe;
					move NewMove;

					if (l == 4)
					{
						switch (PieceValue)
						{
						case (1):
						{
							NewMove.piece = ElementType::spy;
							NewMove.move_data = Direction::down;
							NewMove.x = x;
							NewMove.y = y - 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y - 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						case (2):
						{
							NewMove.piece = ElementType::scout;
							NewMove.move_data = Direction::down;
							NewMove.x = x;
							NewMove.y = y - 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y - 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (3):
						{
							NewMove.piece = ElementType::miner;
							NewMove.move_data = Direction::down;
							NewMove.x = x;
							NewMove.y = y - 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y - 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (4):
						{
							NewMove.piece = ElementType::nine;
							NewMove.move_data = Direction::down;
							NewMove.x = x;
							NewMove.y = y - 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y - 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (5):
						{
							NewMove.piece = ElementType::ten;
							NewMove.move_data = Direction::down;
							NewMove.x = x;
							NewMove.y = y - 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y - 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						default:
							break;
						}
					}
					if (l == 3)
					{
						switch (PieceValue)
						{
						case (1):
						{
							NewMove.piece = ElementType::spy;
							NewMove.move_data = Direction::up;
							NewMove.x = x;
							NewMove.y = y + 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y + 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						case (2):
						{
							NewMove.piece = ElementType::scout;
							NewMove.move_data = Direction::up;
							NewMove.x = x;
							NewMove.y = y + 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y + 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (3):
						{
							NewMove.piece = ElementType::miner;
							NewMove.move_data = Direction::up;
							NewMove.x = x;
							NewMove.y = y + 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y + 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (4):
						{
							NewMove.piece = ElementType::nine;
							NewMove.move_data = Direction::up;
							NewMove.x = x;
							NewMove.y = y + 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y + 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (5):
						{
							NewMove.piece = ElementType::ten;
							NewMove.move_data = Direction::up;
							NewMove.x = x;
							NewMove.y = y + 1;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x;
							A_Move.spmr[1][i] = y + 1;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						default:
							break;
						}
					}
					if (l == 2)
					{
						switch (PieceValue)
						{
						case (1):
						{
							NewMove.piece = ElementType::spy;
							NewMove.move_data = Direction::left;
							NewMove.x = x - 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x - 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						case (2):
						{
							NewMove.piece = ElementType::scout;
							NewMove.move_data = Direction::left;
							NewMove.x = x - 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x - 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (3):
						{
							NewMove.piece = ElementType::miner;
							NewMove.move_data = Direction::left;
							NewMove.x = x - 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x - 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (4):
						{
							NewMove.piece = ElementType::nine;
							NewMove.move_data = Direction::left;
							NewMove.x = x - 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x - 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (5):
						{
							NewMove.piece = ElementType::ten;
							NewMove.move_data = Direction::left;
							NewMove.x = x - 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x - 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						default:
							break;
						}
					}

					if (l == 1)
					{
						switch (PieceValue)
						{
						case (1):
						{
							NewMove.piece = ElementType::spy;
							NewMove.move_data = Direction::right;
							NewMove.x = x + 1; 
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x + 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						case (2):
						{
							NewMove.piece = ElementType::scout;
							NewMove.move_data = Direction::right;
							NewMove.x = x + 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x + 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (3):
						{
							NewMove.piece = ElementType::miner;
							NewMove.move_data = Direction::right;
							NewMove.x = x + 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x + 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (4):
						{
							NewMove.piece = ElementType::nine;
							NewMove.move_data = Direction::right;
							NewMove.x = x + 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x + 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}
						case (5):
						{
							NewMove.piece = ElementType::ten;
							NewMove.move_data = Direction::right;
							NewMove.x = x + 1;
							NewMove.y = y;
							Node *NewNode = new Node();
							A_Move.spmr[0][i] = x + 1;
							A_Move.spmr[1][i] = y;
							NewNode->node_setup(A_Move, NewMove, 0, true);
							NewNode->node_eval();
							parent->addChild(NewNode);
							break;
						}

						default:
							break;
						}
					}

					l -= 1;
				}
			}
		}
	}
}