//#pragma once

#include <stdlib.h>
#include <iostream>
#include <string>
#include <random>
#include "player.hpp"
#include "minimaxTree.hpp"
#include "Node.hpp"

class minimaxTree;
class AI : public Player {
private:
    int placement(ElementType e, int x, int ry, int by, int i);

    bool movement(ElementType e, int x, int y, int i);

    COO allies_spmr;
    COO enemy_spmr;
    minimaxTree *mmt;

public:
    // Constructor
    AI(TeamBoard *board): Player(board){};

    // Function used to set up the pieces
    void setup_pieces();

    // Function used to quickly set up pieces
    void setup_pieces(bool quick);

    // Function used to start a turn.
    // The turn ends when the function resolves.
    void start_turn();

    ~AI();
};