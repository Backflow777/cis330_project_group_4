#pragma once

//#include "AI.hpp"
#include "utils.hpp"
#ifndef MINIMAXTREE_H
#define MINIMAXTREE_H
#include "Node.hpp"

class minimaxTree
{

public:
    ~minimaxTree();
    minimaxTree();
    //minimaxTree(int depth, int node_index, bool isMax, int height, int score_list[], int alpha, int beta);
    minimaxTree(Node *Ele);
    void insert_node(Node *New, Node *Parent, bool isMax, int depth);
    bool search_node(COO spmr);
    void build_tree(COO spmr);
    bool IsLeaf(Node *the_node);
    move final_move();
    void build_base(Node *start_point, Node *NewBase);
    void update_COO(bool isMax);

    void build_from(Node *start_node);
    void delete_node(Node *temp);
    void build_tree(bool success);
    bool compare(COO enemy,COO allies);
    // new function to generate move and return as Node*
    void generate_move(Node *parent, bool IsOurTurn);
    move move_eval();
    void search_move(Node *child, int index);
private:
    COO enemy_pieces;
    COO allies_pieces;
    Node *temp;
    int max_score;
    Node *max_score_node;

    // Stored last turn data
    COO MMTPast_Data;

    // Stored current turn data
    COO MMTCurrent_Data;

    // Stored future turn data
    COO MMTFuture_Data;
    
    // Stored movement data
    move MMTMove_Data;

    // Stored root node
    Node *root;

    // the index for searching no.1
    Node *current;
    // the index for searching no.2
    Node *second_floor;
    // the index for searching no.3
    Node *third_floor;
    int max_depth = 3;
    int scores;
};
#endif
