#pragma once
// need to find a way to include this to liabry
//#include "AI.hpp"
#include "utils.hpp"
#ifndef NODE_H
#define NODE_H

#define SURR_ITEMS 9  
#define LOCAL_ITEMS 3
class Node
{
private:
    
    // pointed to the parent node
    // Node * parent;
    
    // a counter for children
    
    // current COO representation of the board
    COO turn_us;
    // current COO representation of the locaitons of the enemy pieces 
    COO turn_foe;

    COO turn;

    // current COO representation of the known pieces
    COO known_piece;

    // decided move from the tree
    struct move piece_movement;

    // calculated score of a given move on a piece
    int piece_score;
    
    // parent node
    Node * parent;
    
    // decided
    bool decision;
public:
    int max_child;
    int NumOfChildren;
    // pointer to the node of child array
    Node ** child_array;
    // node constructor
    Node();
    Node(Node *parent_node,COO current_turn_us, COO current_turn_foe, int num_child, move &movement, int total_score, bool signal);

    // setup node, called duirng construction or on the go
    bool node_setup(Node *parent_node, COO current_turn_us, COO current_turn_foe, move &movement, int total_score, bool signal);
    bool node_setup(COO current_turn, move &movement, int total_score, bool signal);

    //node evaluation function
    void node_eval();
    bool decided(){return decision;}
    move getMove(){return piece_movement;}
    Node* getParent(){return parent;}

    //node compare function
    int node_comp(int x, int y, int arr[SURR_ITEMS][LOCAL_ITEMS]);
    void swap(Node **a, Node **b);
    int partition(int low, int high);
    void QuickSort(int low, int high);
    void addChild(Node *newChild);
    // show how many children
    int ShowChildrenNum();
    bool compare_by_spmr(COO spmr);
    COO show_turn_us();
    COO show_turn_foe();
    int show_score(); 
    // node destructor
    ~Node();

    // the method to get child of the node
    Node **getChildren();
};

#endif