#include <stdlib.h>
#include <iostream>
#include <string>

#include "board.hpp"

// Base board state update function
Element * Board::state(Team team) {
    Element *state = new Element[20];

    // Get the correct sets of pieces
    Element *myPieces;
    Element *theirPieces;
    if (team == Team::red) {
        myPieces = red_pieces;
        theirPieces = blue_pieces;
    } else {
        myPieces = blue_pieces;
        theirPieces = red_pieces;
    }
    
    // Copy my pieces
    for (int i = 0; i < 10; ++i) {
        Element piece = *(myPieces + i);
        *(state + i) = piece;
    }
    // Copy enemy pieces
    for (int i = 0; i < 10; ++i) {
        Element piece = *(theirPieces + i);
        // If the piece is alive, it should be hidden from us
        if (piece.alive) piece.type = ElementType::unknown;
        *(state + 10 + i) = piece;
    }

    return state;
}

// Returns a COO representation of the board state for a specific team
COO Board::spmr(Team team){
    if((red_piece_count[0] || blue_piece_count[0]) == 0) throw std::runtime_error("Invalid Access");
    int cont = 0;
    COO temp = *spm;
    for (int i = 0; i < 10; ++i) {        
        for (int j = 0; j < 10; ++j) {
            if (board[i][j]->alive == true && board[i][j]->team != team)
            {
                temp.spmr[0][cont] = board[i][j]->y;
                temp.spmr[1][cont] = board[i][j]->x;
                temp.spmr[2][cont] = 9;
                ++cont;
            } 
        }
    }
    return temp;
}

COO Board::spmr1(Team team){
    if((red_piece_count[0] || blue_piece_count[0]) == 0) throw std::runtime_error("Invalid Access");
    int cont = 0;
    COO temp = *spm;
    for (int i = 0; i < 10; ++i) {        
        for (int j = 0; j < 10; ++j) {
            if (board[i][j]->alive == true && board[i][j]->team == team)
            {
                temp.spmr[0][cont] = board[i][j]->y;
                temp.spmr[1][cont] = board[i][j]->x;
                temp.spmr[2][cont] = static_cast<int>(board[i][j]->type);
                ++cont;
            } 
        }
    }
    return temp;
}