#include <stdlib.h>
#include <iostream>
#include <string>

#include "board.hpp"

// Helper function: add deadzones to 10x10 board matrix
template <typename T>
void Board::add_deadzones(T ** board, T deadzone_representation) {
    for (int i = 0; i < 2; ++i) {
        // Row where deadzone creation starts (starts at row 4; rows begin at 0)
        for (int j = 4; j < 6; ++j) {
            // Column where deadzone creation starts (starts at col 2; cols begin at 0)
            int col_offset = (4 * i); // For the second deadzone, shift over 4 columns
            for (int k = 2 + col_offset; k < 4 + col_offset; ++k) {
                *(*(board + j) + k) = deadzone_representation;
            }
        }
    }
}

// Board constructor
Board::Board() {
    // Initialize static board elements
    empty = new Element;
    empty->type = ElementType::empty;

    deadzone = new Element;
    deadzone->type = ElementType::deadzone;

    // Initialize the board
    board = matrix_utils::allocate_matrix<Element *>(10, 10, empty);
    add_deadzones<Element *>(board, deadzone);

    // Allocate the placement counts
    red_piece_count = new int[11]();
    blue_piece_count = new int[11]();

    // Allocate team pieces arrays
    red_pieces = new Element[10]();
    blue_pieces = new Element[10]();

    // Initialize COO of SPM arrays
    spm = new COO();
}

// Board destructor
Board::~Board() {
    // Free malloc'd structs
    delete empty;
    delete deadzone;
    delete spm;

    // Free piece arrays
    delete[] red_pieces;
    delete[] blue_pieces;

    delete[] red_piece_count;
    delete[] blue_piece_count;
    
    // Free matrix
    matrix_utils::free_matrix<Element *>(10, board);
}

// Prints the board's full state for human viewing
void Board::print_state() {
    // Print x coordinates
    std::cout << "    ";
    for (int i = 0; i < 10; ++i) std::cout << i << "   ";
    std::cout << std::endl << std::endl;

    // Print rows
    for (int i = 0; i < 10; ++i) {
        // Print y coordinate
        std::cout << i << "   ";
        // Print pieces
        for (int j = 0; j < 10; ++j) {
            Element *e = *(*(board + i) + j);
            if (e->type == ElementType::empty) {
                std::cout << "--  ";
            } else if (e->type == ElementType::deadzone) {
                std::cout << "XX  ";
            } else if (e->team == Team::red) {
                std::cout << 'R' << e->type << "  ";
            } else if (e->team == Team::blue) {
                std::cout << 'B' << e->type << "  ";
            }
        }
        std::cout << std::endl << std::endl;
    }
}

void Board::print_state(Team team) {
    // Print x coordinates
    std::cout << "    ";
    for (int i = 0; i < 10; ++i) std::cout << i << "   ";
    std::cout << std::endl << std::endl;

    // Print rows
    for (int i = 0; i < 10; ++i) {
        // Print y coordinate
        std::cout << i << "   ";
        // Print pieces
        for (int j = 0; j < 10; ++j) {
            Element *e = *(*(board + i) + j);
            if (e->type == ElementType::empty) {
                std::cout << "--  ";
            } else if (e->type == ElementType::deadzone) {
                std::cout << "XX  ";
            } else {
                char t = (e->team == Team::red) ? 'R' : 'B';
                if (e->team == team) {
                    std::cout << t << e->type << "  ";
                } else {
                    std::cout << t << "?  ";
                }
            }
        }
        std::cout << std::endl << std::endl;
    }
}

// Prints a team specific, size 20 state array
void Board::print_state(Element *state) {
    for (int i = 0; i < 20; ++i) {
        Element piece = *(state + i);
        if (piece.team == Team::none) continue; // Skip unassigned pieces
        char team = (piece.team == Team::red) ? 'R' : (piece.team == Team::blue) ? 'B' : '?';
        std::cout << team << piece.type << " " << piece.x << ", " << piece.y << std::endl;
    }
}

// Prints the dead pieces
void Board::print_dead() {
    std::cout << "Eliminated Pieces" << std::endl;
    int i;
    std::cout << "R: ";
    for (i = 0; i < 10; ++i) {
        Element *p = red_pieces + i;
        if (p->alive) continue;
        std::cout << p->type << " ";
    }
    std::cout << std::endl;
    std::cout << "B: ";
    for (i = 0; i < 10; ++i) {
        Element *p = blue_pieces + i;
        if (p->alive) continue;
        std::cout << p->type << " ";
    }
    std::cout << std::endl;
}

// Checks win conditions
bool Board::check_winner() {
    if (flag_captured == Team::red || blue_mobile == 0) {
        std::cout <<  "Red wins!" << std::endl;
        return true;
    } else if (flag_captured == Team::blue || red_mobile == 0) {
        std::cout <<  "Blue wins!" << std::endl;
        return true;
    } else if (flag_captured == Team::both || (red_mobile + blue_mobile) == 0) {
        std::cout <<  "Draw" << std::endl;
        return true;
    }
    return false;
}