#include <stdlib.h>
#include <iostream>
#include <string>

#include "board.hpp"

// Checks to see if a piece can be placed 
bool Board::check_if_placeable(Team team, ElementType piece) {
    // Get reference to count array
    int *piece_count = (team == Team::red) ? red_piece_count : blue_piece_count;
    // If there is more then ten pieces total, return
    if (*piece_count == 10) return false;
    // Otherwise, get the maximum allowed pieces for the given piece type
    int max_count = (piece == ElementType::bomb || piece == ElementType::miner || piece == ElementType::scout)
        ? 2
        : 1;
    // Check if there is already the max amount
    int count = *(piece_count + static_cast<int>(piece));
    return (count < max_count) ? true : false;
}

// Base placement function
bool Board::place(Team team, ElementType piece, int x, int y) {
    // Check for bad x/y coordinates
    if ((team == Team::red && (x > 9 || y > 2 || x < 0 || y < 0))
        || (team == Team::blue && (x > 9 || y > 9 || y < 7 || x < 0 || y < 0))) {
        return false;
    }

    // Get current space to check if it is empty
    Element *e = *(*(board + y) + x);
    bool placeable = (e->type == ElementType::empty) && check_if_placeable(team, piece);

    if (placeable) {
        // Point to the correct piece count
        int *piece_count = (team == Team::red) ? red_piece_count : blue_piece_count;
        
        // Create the piece element
        Element *piece_element = (team == Team::red)
            ? (red_pieces + *piece_count)
            : (blue_pieces + *piece_count);
        piece_element->alive = true;
        piece_element->team = team;
        piece_element->type = piece;
        piece_element->x = x;
        piece_element->y = y;

        // Place the piece on the board
        *(*(board + y) + x) = piece_element;

        // Increment the total piece count
        *piece_count += 1;
        // Increment the specific piece count
        *(piece_count + static_cast<int>(piece)) += 1;
    }

    return placeable;
}