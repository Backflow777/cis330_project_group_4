CIS 330 Stratego AI Project Board
================================

This repository contains the C++ code for the board to be used by the AI in a game of Stratego. The board has the standard layout, but only supports the Duel Variant pieces and setup conditions.

The AI will be directly imported by the board during the game; for testing and developing AI, please fork this repository.

### Compiling
Assuming `make` is installed, simply run the command ./main in this directory.
To clean up intermediate files, run `make clean`

### Latest News
    
We did not make it. Several werid bugs appeared at very last moment. The minimaxTree class will build<br> way too many nodes which cause memory leak. Function build_from(int minimaxtree class) may build<br> empty node. Serveral functions used wrong constructor.<br>                                                   
    After we tried to fix these parts, unfortunately we found that the game part which is the game<br> board has several weird bugs caused we cannot update our board data. This is the most crucial bug<br> that we were facing, since this board had built for more than a month, and we ran it without any<br> error report, we made a mistake to assume it is fully funtional. However, the board bug<br> left us no time to fix it. We have to regretly announce that we failed this project.<br>


    